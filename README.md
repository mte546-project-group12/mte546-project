# MTE546 Project

## Loading the dataset

1. Download Jester dataset and extract the files following the instructions
2. Place the dataset somewhere of your choosing, e.g. `Jester`
3. Move the videos to `Jester/rgb` and the labels to `Jester/labels`
4. Change working directory to `MFF-pytorch`
5. Create virtual environment and install `requirements.txt`
6. Edit `process_dataset.py` to use the path of your dataset
7. Generate metafiles by preprocessing the dataset `python3 process_dataset.py`
8. Edit `PAN-PyTorch/ops/dataset_config.py` to use the path of your dataset and generated metafiles

## PAN PyTorch

### Model Training
To train the models, run `PAN-PyTorch/scripts/train/20bn/Lite.sh`
#### Single model
In `PAN-PyTorch/ops/dataset.py` ensure line 124 is commented out to train on the entire dataset
```
self.video_list = self.video_list[a:b]
```
#### Multiple non-overlapping models
In `PAN-PyTorch/ops/dataset.py` ensure line 124 not commented out to train on a subset of the entire dataset. For each sub_model, change the `BAG_NUM` value to match the sub-model. The current method uses 5 sub_models, so modify the test split to accomodate different number of models.  
In `PAN-PyTorch/main.py`, modify the store name to a custom name.

### Model Testing
To test models, run `PAN-PyTorch/scripts/test/20bn/Lite.sh`  
In `PAN-PyTorch/test.py`: modify `bag_size` to match the number of models that will be run against eachother. In `checkpoints` definition, add the relevant checkpoints that are saved after training. Modify the store name to be unique to access the logs.  
The `checkpoints` folder contains the best models from our experiments.  
In `PAN-PyTorch/ops/dataset.py` ensure there is no dataset split.

#### Testing Methods
In `PAN-PyTorch/test.py`, the accuracy measures are defined on this line:
```
[prec1, prec5], conf = accuracy_owa(output, target, topk=(1, 5), alpha=args.lr)
```
different accuracy measurement methods are proivided in `PAN-PyTorch/ops/utils.py`
