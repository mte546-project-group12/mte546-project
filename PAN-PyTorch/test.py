# Code for paper:
# [Title]  - "PAN: Towards Fast Action Recognition via Learning Persistence of Appearance"
# [Author] - Can Zhang, Yuexian Zou, Guang Chen, Lei Gan
# [Github] - https://github.com/zhang-can/PAN-PyTorch

import copy

import sys
import os
import time
import shutil
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
from torch.nn.utils import clip_grad_norm_

from ops.dataset import PANDataSet
from ops.models import PAN
from ops.transforms import *
from opts import parser
from ops import dataset_config
from ops.utils import AverageMeter, accuracy, accuracy_owa, accuracy_voting, accuracy_average
from ops.temporal_shift import make_temporal_pool

from tensorboardX import SummaryWriter

best_prec1 = 0

bag_size = 1

checkpoints = [
    "checkpoint/PAN_Lite_jester_resnet50_shift8_blockres_avg_segment8_e20/ckpt_19.pth.tar"
    # "checkpoint/PAN_Lite_jester_resnet50_shift8_blockres_avg_segment8_e20_0/ckpt_19.pth.tar",
    # "checkpoint/PAN_Lite_jester_resnet50_shift8_blockres_avg_segment8_e20_1/ckpt_19.pth.tar",
    # "checkpoint/PAN_Lite_jester_resnet50_shift8_blockres_avg_segment8_e20_2/ckpt_19.pth.tar",
    # "checkpoint/PAN_Lite_jester_resnet50_shift8_blockres_avg_segment8_e20_3/ckpt_19.pth.tar",
    # "checkpoint/PAN_Lite_jester_resnet50_shift8_blockres_avg_segment8_e24_4/ckpt_19.pth.tar"
]

def main():
    
    global args, best_prec1
    args = parser.parse_args()

    if args.base == 'TSM':
        args.shift = True
        args.shift_div = 8
        args.shift_place = 'blockres'

    num_class, args.train_list, args.val_list, args.root_path, prefix = dataset_config.return_dataset(args.dataset,
                                                                                                      args.modality)

    full_arch_name = args.arch
    if args.shift:
        full_arch_name += '_shift{}_{}'.format(args.shift_div, args.shift_place)
    if args.temporal_pool:
        full_arch_name += '_tpool'
    args.store_name = '_'.join(
        ['PAN', args.modality, args.dataset, full_arch_name, args.consensus_type, 'segment%d' % args.num_segments,
         'e{}'.format(args.epochs)])
    if args.pretrain != 'imagenet':
        args.store_name += '_{}'.format(args.pretrain)
    if args.lr_type != 'step':
        args.store_name += '_{}'.format(args.lr_type)
    if args.dense_sample:
        args.store_name += '_dense'
    if args.non_local > 0:
        args.store_name += '_nl'
    if args.suffix is not None:
        args.store_name += '_{}'.format(args.suffix)

    args.store_name += "_meowa"
    print('- storing name: ' + args.store_name)

    check_rootfolders()

    if args.modality == 'RGB':
        data_length = 1
    elif args.modality in ['PA', 'Lite']:
        data_length = 4
    elif args.modality in ['Flow', 'RGBDiff']:
        data_length = 5

    print("-"*30)
    print("Environment Versions:")
    print("- Python: {}".format(sys.version))
    print("- PyTorch: {}".format(torch.__version__))
    print("- TorchVison: {}".format(torchvision.__version__))

    args_dict = args.__dict__
    print("-"*30)
    print("PAN Configurations:")
    print(args_dict)
    print("-"*30)

    models = []
    best = []
    for i in range(bag_size):
        models.append(
            PAN(num_class, args.num_segments, args.modality,
                    base_model=args.arch,
                    consensus_type=args.consensus_type,
                    dropout=args.dropout,
                    img_feature_dim=args.img_feature_dim,
                    partial_bn=not args.no_partialbn,
                    pretrain=args.pretrain,
                    is_shift=args.shift, shift_div=args.shift_div, shift_place=args.shift_place,
                    fc_lr5=not (args.tune_from and args.dataset in args.tune_from),
                    temporal_pool=args.temporal_pool,
                    non_local=args.non_local, data_length=data_length, has_VAP=args.VAP)
        )
        
        #print(model)
        
        crop_size = models[i].crop_size
        scale_size = models[i].scale_size
        input_mean = models[i].input_mean
        input_std = models[i].input_std
        policies = models[i].get_optim_policies()
        train_augmentation = models[i].get_augmentation(flip=False if 'something' in args.dataset or 'jester' in args.dataset else True)

        models[i] = torch.nn.DataParallel(models[i], device_ids=args.gpus).cuda()

        optimizer = torch.optim.SGD(policies,
                                    args.lr,
                                    momentum=args.momentum,
                                    weight_decay=args.weight_decay)

        if args.temporal_pool:  # early temporal pool so that we can load the state_dict
            make_temporal_pool(models[i].module.base_model, args.num_segments)
        if os.path.isfile(checkpoints[i]):
            print(("=> loading checkpoint '{}'".format(checkpoints[i])))
            checkpoint = torch.load(checkpoints[i])
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            best.append(best_prec1)
            models[i].load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print(("=> loaded checkpoint '{}' (epoch {})"
                .format(args.evaluate, checkpoint['epoch'])))
        else:
            print(("=> no checkpoint found at '{}'".format(checkpoints[i])))

        if args.tune_from:
            print(("=> fine-tuning from '{}'".format(args.tune_from)))
            sd = torch.load(args.tune_from)
            sd = sd['state_dict']
            model_dict = models[i].state_dict()
            replace_dict = []
            for k, v in sd.items():
                if k not in model_dict and k.replace('.net', '') in model_dict:
                    print('=> Load after remove .net: ', k)
                    replace_dict.append((k, k.replace('.net', '')))
            for k, v in model_dict.items():
                if k not in sd and k.replace('.net', '') in sd:
                    print('=> Load after adding .net: ', k)
                    replace_dict.append((k.replace('.net', ''), k))

            for k, k_new in replace_dict:
                sd[k_new] = sd.pop(k)
            keys1 = set(list(sd.keys()))
            keys2 = set(list(model_dict.keys()))
            set_diff = (keys1 - keys2) | (keys2 - keys1)
            print('#### Notice: keys that failed to load: {}'.format(set_diff))
            if args.dataset not in args.tune_from:  # new dataset
                print('=> New dataset, do not load fc weights')
                sd = {k: v for k, v in sd.items() if 'fc' not in k}
            if args.modality == 'Flow' and 'Flow' not in args.tune_from:
                sd = {k: v for k, v in sd.items() if 'conv1.weight' not in k}
            model_dict.update(sd)
            models[i].load_state_dict(model_dict)

        if args.temporal_pool and not args.resume:
            make_temporal_pool(models[i].module.base_model, args.num_segments)
    
    models = [m for _, m in sorted(zip(best, models), reverse=True)]

    cudnn.benchmark = True

    # Data loading code
    if args.modality != 'RGBDiff':
        normalize = GroupNormalize(input_mean, input_std)
    else:
        normalize = IdentityTransform()
        
    val_loader = torch.utils.data.DataLoader(
        PANDataSet(args.root_path, args.val_list, num_segments=args.num_segments,
                new_length=data_length,
                modality=args.modality,
                image_tmpl=prefix,
                random_shift=False,
                transform=torchvision.transforms.Compose([
                    GroupScale(int(scale_size)),
                    GroupCenterCrop(crop_size),
                    Stack(roll=(args.arch in ['BNInception', 'InceptionV3'])),
                    ToTorchFormatTensor(div=(args.arch not in ['BNInception', 'InceptionV3'])),
                    normalize,
                ]), dense_sample=args.dense_sample, is_lmdb=args.lmdb),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)
    
    test_file = '/home/w29qin/Jester/file_lists/test_videofolder.txt'
    test_loader = torch.utils.data.DataLoader(
        PANDataSet(args.root_path, test_file, num_segments=args.num_segments,
                new_length=data_length,
                modality=args.modality,
                image_tmpl=prefix,
                random_shift=False,
                transform=torchvision.transforms.Compose([
                    GroupScale(int(scale_size)),
                    GroupCenterCrop(crop_size),
                    Stack(roll=(args.arch in ['BNInception', 'InceptionV3'])),
                    ToTorchFormatTensor(div=(args.arch not in ['BNInception', 'InceptionV3'])),
                    normalize,
                ]), dense_sample=args.dense_sample, is_lmdb=args.lmdb),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    # define loss function (criterion) and optimizer
    if args.loss_type == 'nll':
        criterion = torch.nn.CrossEntropyLoss().cuda()
    else:
        raise ValueError("Unknown loss type")

    print("-"*30)
    for group in policies:
        print(('group: {} has {} params, lr_mult: {}, decay_mult: {}'.format(
            group['name'], len(group['params']), group['lr_mult'], group['decay_mult'])))
    print("-"*30)
    
    master_log = open('/home/w29qin/mte546-project/PAN-PyTorch/log/log.csv', 'a')

    log_training = open(os.path.join(args.root_log, args.store_name, 'log.csv'), 'a')
    with open(os.path.join(args.root_log, args.store_name, 'args.txt'), 'w') as f:
        f.write(str(args))
    tf_writer = SummaryWriter(log_dir=os.path.join(args.root_log, args.store_name))

    # VALIDATE
    prec1 = test(test_loader, models, criterion, 0, log_training, tf_writer)

    # remember best prec@1 and save checkpoint
    best_prec1 = max(prec1, best_prec1)
    tf_writer.add_scalar('acc/test_top1_best', best_prec1, 0)

    output_best = 'Best Prec@1: %.3f\n' % (best_prec1)
    print(output_best)
    
    log_training.write(output_best + '\n')
    log_training.flush()

def test(val_loader, models, criterion, epoch, log=None, tf_writer=None):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to evaluate mode
    for model in models:
        model.eval()

    confusion = np.zeros((27, 27))
    
    end = time.time()
    with torch.no_grad():
        for i, (input, target) in enumerate(val_loader):
            target = target.cuda()

            # compute outputs
            output = [model(input) for model in models]

            loss = criterion(output[0], target)

            # measure accuracy and record loss
            # [prec1, prec5], conf = accuracy_owa(output, target, topk=(1, 5), alpha=0.7)
            [prec1, prec5], conf = accuracy(output[0], target, topk=(1, 5))

            confusion += conf
            
            losses.update(loss.item(), input.size(0))
            top1.update(prec1.item(), input.size(0))
            top5.update(prec5.item(), input.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                output = ('Test: [{0}/{1}]\t'
                          'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                          'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                          'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                          'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                    i, len(val_loader), batch_time=batch_time, loss=losses,
                    top1=top1, top5=top5))
                print(output)
                if log is not None:
                    log.write(output + '\n')
                    log.flush()
    print("CONFUSION")
    for row in confusion:
        ss = [str(int(s)) for s in row]
        print(",".join(ss))
    
    

    output = ('Testing Results: Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f} Loss {loss.avg:.5f}'
              .format(top1=top1, top5=top5, loss=losses))
    print(output)
    if log is not None:
        log.write(output + '\n')
        log.flush()

    if tf_writer is not None:
        tf_writer.add_scalar('loss/test', losses.avg, epoch)
        tf_writer.add_scalar('acc/test_top1', top1.avg, epoch)
        tf_writer.add_scalar('acc/test_top5', top5.avg, epoch)

    return top1.avg


def check_rootfolders():
    """Create log and model folder"""
    folders_util = [args.root_log, args.root_model,
                    os.path.join(args.root_log, args.store_name),
                    os.path.join(args.root_model, args.store_name)]
    for folder in folders_util:
        if not os.path.exists(folder):
            print('creating folder ' + folder)
            os.mkdir(folder)

if __name__ == '__main__':
    main()
