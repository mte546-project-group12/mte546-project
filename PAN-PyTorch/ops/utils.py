# Code for paper:
# [Title]  - "PAN: Towards Fast Action Recognition via Learning Persistence of Appearance"
# [Author] - Can Zhang, Yuexian Zou, Guang Chen, Lei Gan
# [Github] - https://github.com/zhang-can/PAN-PyTorch

import numpy as np
import torch


def softmax(scores):
    es = np.exp(scores - scores.max(axis=-1)[..., None])
    return es / es.sum(axis=-1)[..., None]


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    
    confusion = np.zeros((27, 27))
    for (p, t) in zip(pred[0], target):
        confusion[p][t] += 1
    
    correct = pred.eq(target.view(1, -1).expand_as(pred))
    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res, confusion

def accuracy_average(outputs, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)
    
    output = torch.zeros_like(outputs[0])
    for o in outputs:
        output += o
    
    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    
    confusion = np.zeros((27, 27))
    for (p, t) in zip(pred[0], target):
        confusion[p][t] += 1
    
    correct = pred.eq(target.view(1, -1).expand_as(pred))
    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    

        
    return res, confusion

def accuracy_owa(outputs, target, topk=(1,), alpha=0.5):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)
    
    output = torch.zeros_like(outputs[0])
    weights = torch.tensor(meowa(alpha))
    for i, o in enumerate(outputs):
        output = output + o * weights[i]
    
    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    
    confusion = np.zeros((27, 27))
    for (p, t) in zip(pred[0], target):
        confusion[p][t] += 1
    
    correct = pred.eq(target.view(1, -1).expand_as(pred))
    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res, confusion

def accuracy_voting(outputs, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    ps = []
    for output in outputs:
        _, p = output.data.topk(maxk, 1, True, True)
        ps.append(p.t())
    combined_tensor = torch.stack(ps)
    pred = torch.zeros_like(ps[0])
    for i in range(pred.size(0)):
        for j in range(pred.size(1)):
            counts = torch.bincount(combined_tensor[:, i, j].flatten(), minlength=10)
            most_frequent_number = torch.argmax(counts)
            pred[i, j] = most_frequent_number

    confusion = np.zeros((27, 27))
    for (p, t) in zip(pred[0], target):
        confusion[p][t] += 1

    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res, confusion

def meowa(alpha):
    n = 5
    a = -alpha * (n-1)
    coeffs = [
        a,
        a+1,
        a+2,
        a+3,
        a+4
    ]
    
    t = np.real(np.max(np.roots(coeffs)))
    
    den = sum([pow(t, i) for i in range(n)])
    return [
        round(pow(t, i)/ den, 4) for i in range(n)
    ]
