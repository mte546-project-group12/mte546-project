import os
import torch
import torchvision
import torchvision.datasets as datasets


def return_jester(modality):
    filename_categories = 'file_lists/category.txt'
    filename_imglist_train = 'file_lists/train_videofolder.txt'
    filename_imglist_val = 'file_lists/val_videofolder.txt'
    root_data = '/home/w29qin/Jester/'
    if modality == 'RGB':
        prefix = '{:05d}.jpg'
    elif modality == 'RGBFlow':
        prefix = '{:05d}.jpg'
    else:
        print('no such modality:'+modality)
        os.exit()
    return filename_categories, filename_imglist_train, filename_imglist_val, root_data, prefix

def return_nvgesture(modality):
    filename_categories = 'nvgesture/category.txt'
    filename_imglist_train = 'nvgesture/train_videofolder.txt'
    filename_imglist_val = 'nvgesture/val_videofolder.txt'
    root_data = '/data2/nvGesture'
    if modality == 'RGB':
        prefix = '{:05d}.jpg'
    elif modality == 'RGBFlow':
        prefix = '{:05d}.jpg'
    else:
        print('no such modality:'+modality)
        os.exit()
    return filename_categories, filename_imglist_train, filename_imglist_val, root_data, prefix

def return_chalearn(modality):
    filename_categories = 'chalearn/category.txt'
    filename_imglist_train = 'chalearn/train_videofolder.txt'
    filename_imglist_val = 'chalearn/val_videofolder.txt'
    #filename_imglist_val = 'chalearn/test_videofolder.txt'
    root_data = '/data2/ChaLearn'
    if modality == 'RGB':
        prefix = '{:05d}.jpg'
    elif modality == 'RGBFlow':
        prefix = '{:05d}.jpg'
    else:
        print('no such modality:'+modality)
        os.exit()
    return filename_categories, filename_imglist_train, filename_imglist_val, root_data, prefix

def return_dataset(dataset, modality):
    dict_single = {'jester':return_jester, 'nvgesture': return_nvgesture, 'chalearn': return_chalearn}
    if dataset in dict_single:
        file_categories, file_imglist_train, file_imglist_val, root_data, prefix = dict_single[dataset](modality)
    else:
        raise ValueError('Unknown dataset '+dataset)

    file_imglist_train = os.path.join(root_data, file_imglist_train)
    file_imglist_val = os.path.join(root_data, file_imglist_val)
    file_categories = os.path.join(root_data, file_categories)
    with open(file_categories) as f:
        lines = f.readlines()
    categories = [item.rstrip() for item in lines]
    return categories, file_imglist_train, file_imglist_val, root_data, prefix

